import java.util.Scanner;

public class Calculadora {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Introdueix el primer número: ");
		double num1 = scanner.nextDouble();

		System.out.println("Introdueix el segon número: ");
		double num2 = scanner.nextDouble();

		System.out.println("Suma: " + suma(num1, num2));
		System.out.println("Resta: " + restar(num1, num2));
		System.out.println("Multiplicació: " + multiplicar(num1, num2));
		try {
            		System.out.println("Divisió: " + Operacions.dividir(num1, num2));
        	} catch (ArithmeticException e) {
            		System.out.println("Error: " + e.getMessage());
       		 } finally {
           		 scanner.close(); 
        	}
	}

	public static double suma(double num1, double num2) {
		return num1 + num2;
	}
	public static double restar(double a, double b) {
		return a - b;
	}
	public static double multiplicar(double a, double b) {
		return a * b;
	}
	public static double dividir(double a, double b) throws ArithmeticException {
		if (b == 0) {
			throw new ArithmeticException("El divisor no pot ser zero");
		}
		return a / b;
	}
}

